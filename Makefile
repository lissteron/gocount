APP = gocount

golint:
	golangci-lint run -v

test:
	echo 'https://golang.org\nhttps://golang.org' | go run main.go