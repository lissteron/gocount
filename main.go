package main

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"sync"
	"sync/atomic"
)

const (
	threads = 5
)

func main() {
	var (
		wg        = new(sync.WaitGroup)
		reader    = bufio.NewReader(os.Stdin)
		limitChan = make(chan struct{}, threads)
		goReg     = regexp.MustCompile(`(^|\W+)Go(\W+|$)`)
		total     int64
	)

	for {
		url, err := reader.ReadString('\n')
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}

			log.Fatal(err)
		}

		limitChan <- struct{}{}
		wg.Add(1)

		go getHTTP(wg, limitChan, strings.TrimSuffix(url, "\n"), goReg, &total)
	}

	wg.Wait()

	log.Printf("Total: %d\n", total)
}

func getHTTP(wg *sync.WaitGroup, limitChan chan struct{}, url string, goReg *regexp.Regexp, total *int64) {
	defer func() {
		wg.Done()
		<-limitChan
	}()

	resp, err := http.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		log.Printf("can't get url: %v. %s", err, url)
		return
	}

	if resp.StatusCode != 200 {
		log.Printf("bad status code: %d, %s", resp.StatusCode, url)
		return
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("can't read body: %v. %s", err, url)
		return
	}

	count := len(goReg.FindAll(b, -1))

	log.Printf("Count for %s: %d\n", url, count)

	atomic.AddInt64(total, int64(count))
}
